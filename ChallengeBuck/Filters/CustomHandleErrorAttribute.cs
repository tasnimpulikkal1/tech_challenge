﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ChallengeBuck.Models;

namespace ChallengeBuck.Filters
{
    public class CustomHandleErrorAttribute : HandleErrorAttribute
    {
        //check whether it is an ajax exception
        private bool IsAjax(ExceptionContext filterContext)
        {
            return filterContext.HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
        }
        // Controller wide exception. all application related exceptions are handled here
        public override void OnException(ExceptionContext filterContext)
        {
            
            
            // if the request is AJAX return JSON else view.
            if (IsAjax(filterContext))
            {
                //Because its a exception raised after ajax invocation
                //Lets return Json
                filterContext.Result = new JsonResult()
                {
                    Data = filterContext.Exception.Message,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };

                filterContext.ExceptionHandled = true;
                filterContext.HttpContext.Response.Clear();
            }
            else
            {
                //Normal Exception
                //So let it handle by its default ways.
                base.OnException(filterContext);

            }
            HttpException httpException;
           
                //httpException = new HttpException(null, new Exception("We cannot process your request at this time."));
           httpException = new HttpException(null, filterContext.Exception);
           
            //Get the HTTPCode for the page 
            int statusCode = httpException.GetHttpCode();

            filterContext.ExceptionHandled = true;
            //return to the error page
            filterContext.Result = new ViewResult()
            {
                ViewName = "Error"

            };
            filterContext.HttpContext.Response.Clear();
            
          
            
        }
    }
}