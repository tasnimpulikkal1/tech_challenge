﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace ChallengeBuck.Controllers
{
    public class ErrorController:Controller
    {
        public ActionResult Index(HandleErrorInfo errorInfo)
        {
            return View();
        }

        public ActionResult NotFound()
        {
            return View();
        }

        public ActionResult Unknown()
        {
            return View();
        }
    }
}