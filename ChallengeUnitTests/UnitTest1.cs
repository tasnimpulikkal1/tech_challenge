﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using ChallengeBuck.Controllers;
using ChallengeBuck.Api;
using ChallengeBuck.Models;

namespace ChallengeUnitTests
{
    [TestClass]
    public class UnitTest1
    {

        [TestMethod]
        public void Index()
        {
            ChallengeBuck.Controllers.TitleController titlecontroller = new ChallengeBuck.Controllers.TitleController();
            // checking if it is returning a view result
            ViewResult result = titlecontroller.Index() as ViewResult;

            // Do an Assert
            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void TestMethod1()
        {
            //testing code comes here , check for expecting value CasaBlanca, search in the api and return a single titlename from the list.
            //do an assert statement to verify         
            ChallengeBuck.Api.TitleController titleapicontroller = new ChallengeBuck.Api.TitleController();
            string expectingValue = titleapicontroller.Search("Casablanca").ToList().First().TitleName.ToString();
            Assert.AreEqual("Casablanca", expectingValue);
        }
    }
}
