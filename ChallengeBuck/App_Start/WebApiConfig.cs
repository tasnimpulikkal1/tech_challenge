﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ChallengeBuck.Models;
using System.Web.Http;

namespace ChallengeBuck
{
    public class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Filters.Add(new HExceptionFilter());
        }
    }
}