﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChallengeBuck.Models
{
    public class ErrorViewModel
    {
        public ErrorViewModel(HttpException exception, string controllerName, string actionName)
        {
            this.ActionName = actionName;
            this.ControllerName = controllerName;
            this.Exception = exception;
        }

        public string ActionName { get; private set; }
        public string ControllerName { get; private set; }
        public HttpException Exception { get; private set; }
    }
}