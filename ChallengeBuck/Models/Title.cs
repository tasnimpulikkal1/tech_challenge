﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ChallengeBuck.Models
{
    [Table("Title")]
    public class Title
    {
        [Key()]
        //specify the Model acc to the DB Table
        //I have used a model first approach here
        //All Validation logic comes here
        public int TitleId { get; set; }
        public string TitleName { get; set; }
        public string TitleNameSortable { get; set; }
        public Nullable<int> ReleaseYear { get; set; }
        //specify the datetime field 
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MM-dd-yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> ProcessedDateTimeUTC { get; set; }
    }
}