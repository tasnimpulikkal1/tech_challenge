﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ChallengeBuck.Models;
using ChallengeBuck.DBContext;

namespace ChallengeBuck.Api
{
    public class TitleController : ApiController
    {
        [HttpGet]
        [HExceptionFilter]
        public IEnumerable<Title> Search(string searchkey)
        {
            //get all the values for the search key and stores into titles
            List<Title> _titles = new List<Title>();

            try
            {
                _titles = new UserContext().Titles.Where(x => x.TitleName.Contains(searchkey)).ToList();
              //throw an exception if object is null
                if (_titles == null)
                {
                    HttpResponseMessage msg = new HttpResponseMessage(HttpStatusCode.NotFound)
                    {
                        Content = new StringContent(string.Format("No Title with TitleName = {0}", searchkey)),
                        ReasonPhrase = "Title Not Found in Database!"
                    };
                    
                   // throw new HttpResponseException(msg);                    
                }
            }
            catch (Exception ex)
            {
                //throw a status code not found exception
                HttpResponseMessage msg2 = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(ex.Message.ToString()),
                    ReasonPhrase = "Exception"
                };

                   
                    
               // throw new Exception(ex.Message);
               
            }

                

            return _titles;
            
        }
        [HttpGet]
        public string Get()
         {
            //test method for api
             return DateTime.Now.ToString();
        }
       
    }
}