﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using ChallengeBuck.Models;
namespace ChallengeBuck.DBContext
{
    public class UserContext : DbContext
    {
        public UserContext()
            : base("name=TitlesDbContext")
        {
        }

        public DbSet<Title> Titles { get; set; }
    }
}