(function () {
    var app = angular.module('title_search', []);
    app.controller('TitleController', ['$scope', '$http', function ($scope, $http) {
        $scope.searchkey='',
        $scope.Titles = {};
        $scope.searchEmployee = function () {
            if ($scope.searchkey != '') {
                $http({ method: 'GET', url: '/../api/title/search/?searchkey=' + $scope.searchkey }).
                      success(function (data, status) {
                         // if(status==)
                          $scope.Titles = data;
                      }).error(function (data, status, headers, config) {
                          alert(status);
                      });
            }
        }
    }]);

})();